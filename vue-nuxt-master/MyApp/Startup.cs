using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Funq;
using ServiceStack;
using MyApp.ServiceInterface;
using Microsoft.AspNetCore.Mvc;
using DevRestore;
using TempSignalR;

namespace MyApp
{
  public class Startup : ModularStartup
  {
    public Startup(IConfiguration configuration)
        : base(configuration, typeof(MyServices).Assembly) { }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public new void ConfigureServices(IServiceCollection services)
    {

      //разрешаем кросс-доменные запросы
      services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
      {
        builder.AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader();
      }));

      //подключаем SignalR
      services.AddSignalR(o =>
      {
        o.EnableDetailedErrors = true;
      });

      // для приложения TempSignalR
      services.AddTransient<StockTicker>();

      // для приложения DevRestore
      services.AddTransient<DevDbCommon>();
      services.AddTransient<DevDbRestore>();
      services.AddTransient<IDevDbCommon, DevDbCommon>();
      services.AddTransient<IDevDbRestore, DevDbRestore>();

      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
    }



    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      //маршруты для SignalR
      app.UseSignalR(routes =>
      {
        // разбирался, для приложения TempSignalR, можно удалять
        routes.MapHub<SignalRHub>("/chat");

        // рабочий, для приложения DevRestore
        routes.MapHub<DevHub>("/signalR_devRestore");
      });

      //разрешаем кроссдоменные запросы
      app.UseCors("MyPolicy");

      //фреймворк ServiceStack, пока просто для примера
      app.UseServiceStack(new AppHost
      {
        AppSettings = new NetCoreAppSettings(Configuration)
      });

      app.UseMvc();
    }
  }


  /// <summary>
  /// пока не использую ServiceStack 
  /// </summary>
  public class AppHost : AppHostBase
    {
        public AppHost() : base("MyApp", typeof(MyServices).Assembly) { }

        // Configure your AppHost with the necessary configuration and dependencies your App needs
        public override void Configure(Container container)
        {
            Plugins.Add(new SharpPagesFeature()); // enable server-side rendering, see: https://sharpscript.net/docs/sharp-pages

            SetConfig(new HostConfig
            {
                UseSameSiteCookies = true,
                AddRedirectParamsToQueryString = true,
                DebugMode = AppSettings.Get(nameof(HostConfig.DebugMode), HostingEnvironment.IsDevelopment()),
            });

            Svg.Load(ContentRootDirectory.GetDirectory("/src/assets/svg"));
            Svg.CssFillColor["svg-icons"] = "#2f495e";
        }
    }
}
