using System;
using System.Collections.Generic;
//using DevExtreme.NETCore.Demos.Models.SignalR;
using Microsoft.AspNetCore.SignalR;

namespace TempSignalR
{
  public class SignalRHub : Hub
  {
    private readonly StockTicker _stockTicker;

    public SignalRHub(StockTicker stockTicker)
    {
      _stockTicker = stockTicker;
    }


    
    public IEnumerable<Stock> GetAllStocks(string user, string message)
    {

      IHubCallerClients client = Clients;
      _stockTicker.UserGuid = user;

      return _stockTicker.GetAllStocks(client);

    }
  }
}
