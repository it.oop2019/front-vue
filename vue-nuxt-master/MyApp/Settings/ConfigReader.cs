
using System;
using System.Configuration;
using System.Reflection;
using System.Diagnostics;

namespace Settings
{
  /// <summary>
  /// Класс для чтения конфигурационных файлов .config
  /// </summary>
  internal static class ConfigReader
  {

    /// <summary>
    /// Возвращает значение конфигурационного файла по ключу
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    internal static string GetValueByKey(string key)
    {
      Configuration config = null;
      var exeConfigPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
      string value = string.Empty;

      try
      {
        config = ConfigurationManager.OpenExeConfiguration(exeConfigPath);
        if (config != null)
        {
          var element = config.AppSettings.Settings[key];
          if (element != null)
          {
            value = element.Value;
          }
        }
      }
      catch (Exception ex)
      {
        //
        var er = ex.Message;
      }
      return value;
    }
  }
}
