

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Settings
{
  /// <summary>
  /// Класс App. Управляет другими классами в библиотеке Log.dll
  /// </summary>
  internal class SettingsDB
  {

    // public Dictionary<string, string> Settings = new Dictionary<string, string>(); //
    private SqlConnection _connSettings = null;

    /// <summary>
    /// Конструктор по умолчанию
    /// </summary>
    internal SettingsDB()
    {
      var connString = Settings.ConfigReader.GetValueByKey("Settings.ConnString");
      _connSettings = new SqlConnection(connString);
      _connSettings.Open();

    }

    /// <summary>
    /// Инициализация. Настройки из бд
    /// </summary>
    internal Dictionary<string, string> GetSettingsGlobal()
    {
      Dictionary<string, string> sg = new Dictionary<string, string>();

      // читаем настройки из бд
      try
      {

        var dt = new DataTable();
        var cmd = new SqlCommand($@"SELECT * 
                                            FROM dbo.SettingsGlobal g 
                                            WHERE g.ParentID = 2 ORDER BY g.ID"
                                , _connSettings);

        var da = new SqlDataAdapter(cmd);
        da.Fill(dt);

        // запролняем коллецию настройками
        foreach (DataRow row in dt.Rows)
        {
          var code = row["Code"].ToString();
          var value = row["Value"].ToString();
          sg.Add(code, value);
        }

      }

      catch (Exception ex)
      {

      }

      finally
      {
        //очищаем ресурсы конекта
        if (_connSettings.State == ConnectionState.Open)
        {
          _connSettings.Close();
          _connSettings.Dispose();
        }
      }

      return sg;

    }


  }

}
