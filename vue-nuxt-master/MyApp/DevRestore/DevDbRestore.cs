

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.AspNetCore.SignalR;
using DevRestore.Model;


namespace DevRestore
{
  /// <summary>
  /// Класс DevDbRestore. Восстанавливает новую БД на основе созданного бакапа.
  /// </summary>
  public class DevDbRestore : IDevDbRestore
  {
    private SqlConnection _conn = null;
    private string _pathBackup;
    private string _pathRestore;

    private DevHub _hub;
    private DevDbCommon _devDB;


    /// <summary>
    /// Конструктор по умолчанию
    /// </summary>
    public DevDbRestore(DevDbCommon devDB)
    {
     _pathBackup = Settings.ConfigReader.GetValueByKey("Settings.DevRestore.PathBackup");
     _pathRestore = Settings.ConfigReader.GetValueByKey("Settings.DevRestore.PathRestore");
     var connStr = Settings.ConfigReader.GetValueByKey("Settings.DevRestore.ConnString");
      _conn = new SqlConnection(connStr);
      _conn.Open();
      _devDB = devDB;
    }


    /// <summary>
    /// Восстанавливаем новую БД на sql-сервере. Сначала делваем Backup, затем Restore. Пути прописыаются в файле app.config
    /// </summary>
    /// <param name="hub"></param>
    /// <param name="dbParentName"></param>
    /// <param name="dbNewName"></param>
    /// <param name="backupName"></param>
    public void RestoreDB(DevHub hub, string dbParentName, string dbNewName, string backupName = "backup")
    {


      // на всяк случай проверим 
      if (hub == null) return;
      if (hub.UserID == null) return;
      if (hub.CallerClient == null) return;
      if (string.IsNullOrEmpty(dbParentName)) return;
      if (string.IsNullOrEmpty(dbNewName)) return;

      _hub = hub;


      // проверка на валидность имени новой бд. Если бд с таким именем уже есть - говорим клиенту и выходим
      List<DataBase> dbList = _devDB.GetListDB();
      DataBase findDB = dbList.Find(item => item.Name == dbNewName);
      if (findDB != null || dbNewName.ToLower() == "dwh-dev" || dbNewName.ToLower() == "dwh-test")
      {
        // шлем на фронт событие с сообщением сервера об ошибке
        _hub.CallerClient.Caller.SendAsync("onError", $"База данных с именем '{dbNewName}' уже существует!");
        return;
      }


      var dbForBackup = dbParentName; //имя БД для бакапа
      var backupFile = "";

      // готовим sql для BACKUP
      backupFile = $@"{_pathBackup}\{backupName}.bak";
      var sql = $@"BACKUP DATABASE [{dbForBackup}] TO DISK = '{backupFile}' WITH INIT, STATS = 1";

      using (SqlCommand cmd = new SqlCommand(sql, _conn))
      {
        cmd.CommandTimeout = 3000000;
        _conn.FireInfoMessageEventOnUserErrors = true;
        // добавляем событие сиквела для backup
        _conn.InfoMessage += delegate (object sender, SqlInfoMessageEventArgs e)
        {
          SqlInfoMessage(sender, e, "Backup", dbParentName);
        };

        //стартуем бакап
        cmd.ExecuteNonQuery();
        //удаляем событие
        _conn.InfoMessage -= delegate (object sender, SqlInfoMessageEventArgs e) { };

        // получим список файлов в бакапе
        sql = $@"RESTORE FILELISTONLY FROM DISK = '{backupFile}'";
        cmd.CommandText = sql;
        SqlDataReader reader = cmd.ExecuteReader();

        // готовим sql для RESTORE
        string moveFiles = "";
        while (reader.Read())
        {
          var logicalName = reader.GetString(0);
          var physicalName = reader.GetString(1);
          var fileExtension = physicalName.ToLower().Substring(physicalName.Length - 3);
          //собираем строку для файлов бд
          moveFiles += $@" MOVE '{logicalName}' TO '{_pathRestore}\{dbNewName}-{logicalName}.{fileExtension}',";

        }
        reader.Close();

        sql = $@"RESTORE DATABASE [{dbNewName}]
               FROM DISK = '{backupFile}'
               WITH {moveFiles} STATS = 1";

        //стартуем RESTORE
        cmd.CommandText = sql;
        // добавляем событие сиквела для Restore
        _conn.InfoMessage += delegate (object sender, SqlInfoMessageEventArgs e)
        {
          SqlInfoMessage(sender, e, "Restore", dbNewName);
        };

        cmd.ExecuteNonQuery();
        //удаляем событие
        _conn.InfoMessage -= delegate (object sender, SqlInfoMessageEventArgs e) { };

      }
    }


    /// <summary>
    /// Событие сиквела
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <param name="typeOperation"></param>
    private void SqlInfoMessage(object sender, SqlInfoMessageEventArgs e, string typeSql, string dbNewName)
    {
      string msg = e.Message;
      var err = e.Errors;

      //
      if (msg.Contains("percent processed"))
      {
        // шлем на фронт проценты
        int percent;
        int.TryParse(string.Join("", msg.Where(c => char.IsDigit(c))), out percent);

        var eventName = typeSql == "Backup" ? "onPercentBackup" : "onPercentRestore";
        _hub.CallerClient.Caller.SendAsync(eventName, percent);
        _hub.CallerClient.Caller.SendAsync("onUserID", _hub.UserID, "xxx");

      }
      // шлем на фронт событие с полным сообщением сервера
      _hub.CallerClient.Caller.SendAsync("onMessage", msg, "xxx");
      //_hub.CallerClient.Caller.SendAsync("onError", err, "xxx");


      // шлем на фронт событие успешного завершения Backup
      if (msg.Contains("BACKUP DATABASE successfully"))
      {
        _hub.CallerClient.Caller.SendAsync("onCompleteBackup", $"Backup базы данных '{dbNewName}' успешно создан!");
      }

      // шлем на фронт событие успешного завершения Restore
      if (msg.Contains("RESTORE DATABASE successfully"))
      {
        _hub.CallerClient.Caller.SendAsync("onCompleteRestore", $"База данных '{dbNewName}' успешно восстановлена!");
      }

    }
  }
}
