using System;

namespace DevRestore.Model
{

  /// <summary>
  /// Структура (класс) для списка баз данных
  /// </summary>
  public class DataBase
  {
    public string Name { get; set; }
    public int Database_id { get; set; }
    public string State { get; set; }
  }
}
