

using Microsoft.AspNetCore.SignalR;

namespace DevRestore
{
  public class DevHub : Hub
  {
    public string UserID;
    public IHubCallerClients CallerClient;
    private IDevDbRestore _devDbRestore;

    public DevHub(DevDbRestore devDbRestore)
    {
      _devDbRestore = devDbRestore;
    }




    public void RestoreDB(string userID, string dbParentName, string dbNewName, string dbBackupName= "backup")
    {
      CallerClient = Clients;
      UserID = userID;

      if (this == null) return;

      _devDbRestore.RestoreDB(this, dbParentName, dbNewName, dbBackupName);

    }
  }

}
