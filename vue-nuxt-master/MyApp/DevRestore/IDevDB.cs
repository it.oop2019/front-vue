using DevRestore.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevRestore
{

  /// <summary>
  /// Интерфейс для класса DevDbCommon 
  /// </summary>
  public interface IDevDbCommon
  {
    List<DataBase> GetListDB();

  }
}
