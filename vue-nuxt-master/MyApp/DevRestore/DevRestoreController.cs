
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using DevRestore.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DevRestore
{
  [Route("api/[controller]")]
  [Route("api/[controller]/[action]")]
  [EnableCors("MyPolicy")]
  [ApiController]
  public class DevRestoreController : ControllerBase
  {
    DevDbCommon _devDB;

    public DevRestoreController(DevDbCommon devDB)
    {
      _devDB = devDB;
    }

    // 
    [HttpGet]
    public IEnumerable<DataBase> Get()
    {

      var listDB = _devDB.GetListDB();

      //List<DataBase> listDB = new List<DataBase>
      //{
      //  new DataBase { Name = "MSFT", Database_id = 5, Create_date = DateTime.Now },
      //  new DataBase { Name = "MSFT1", Database_id = 5, Create_date = DateTime.Now },
      //  new DataBase { Name = "MSFT2", Database_id = 5, Create_date = DateTime.Now },
      //  new DataBase { Name = "MSFT3", Database_id = 5, Create_date = DateTime.Now },
      //  new DataBase { Name = "MSFT4", Database_id = 5, Create_date = DateTime.Now }
      //};

      return listDB;

    }



    public string CreateBackup()
    {
      return DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
    }


  }
}
