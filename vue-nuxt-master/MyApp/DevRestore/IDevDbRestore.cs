using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevRestore
{

  /// <summary>
  /// Интерфейс для класса DevDbRestore 
  /// </summary>
  public interface IDevDbRestore
  {
     void RestoreDB(DevHub hub, string dbParentName, string dbNewName, string backupName = "backup");

  }
}
