

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DevRestore.Model;
using Microsoft.AspNetCore.SignalR;
using System.Threading;


namespace DevRestore
{
  /// <summary>
  /// Класс DevDbCommon. Запросы к sql-server
  /// </summary>
  public class DevDbCommon : IDevDbCommon
  {

    private SqlConnection _conn = null;


    /// <summary>
    /// Конструктор по умолчанию
    /// </summary>
    public DevDbCommon()
    {
      var connStr = Settings.ConfigReader.GetValueByKey("Settings.DevRestore.ConnString");
      _conn = new SqlConnection(connStr);
      _conn.Open();
    }


    /// <summary>
    /// Возвращает список баз данных
    /// </summary>
    /// <returns></returns>
    public List<DataBase> GetListDB()
    {
      var result = new List<DataBase>();
      var sql = @"SELECT  NAME
       , database_id
       , CASE STATE
            WHEN 0 THEN 'ONLINE'
           WHEN 1 THEN 'RESTORING'
           WHEN 3 THEN 'RECOVERY_PENDING'
           WHEN 4 THEN 'SUSPECT'
           WHEN 5 THEN 'EMERGENCY'
           WHEN 6 THEN 'RESTORING'
           WHEN 7 THEN 'OFFLINE'
           WHEN 8 THEN 'COPYING'
           WHEN 9 THEN 'RESTORING'
           WHEN 10 THEN 'OFFLINE_SECONDARY'
        END AS STATE
FROM sys.databases d
WHERE d.name  NOT IN ('tempdb','model','msdb', 'master')
ORDER BY NAME     ";

      using (SqlCommand cmd = new SqlCommand(sql, _conn))
      {
        using (SqlDataReader reader = cmd.ExecuteReader())
        {
          while (reader.Read())
          {
            var obj = new DataBase
            {
              Name = reader.GetString(0),
              Database_id = reader.GetInt32(1),
              State = reader.GetString(2)
            };

            result.Add(obj);
          }
        }
      }

      return result;
    }
  }
}
